﻿using System;
using System.Security.Claims;
using Microsoft.Owin.Security.OAuth;

namespace StudentTracking.ServiceHost.API.Auth
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override System.Threading.Tasks.Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context) 
        {
            context.Validated();
            return base.ValidateClientAuthentication(context);
        }

        public override System.Threading.Tasks.Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            if(context.UserName == "Efe" && context.Password == "zambak") {
                
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim("userId", "1"));
                identity.AddClaim(new Claim("identityNumber", "24196282194"));
                identity.AddClaim(new Claim("userType", "Pharmacy"));
                context.Validated(identity);
            } else {
                context.SetError("invalid_grant", "Invalid username or password please contact with system administrator");
            }
            return base.GrantResourceOwnerCredentials(context);
        }
    }
}
