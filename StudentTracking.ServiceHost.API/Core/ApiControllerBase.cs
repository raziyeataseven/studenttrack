﻿using System;
using System.Web.Http;

namespace StudentTracking.ServiceHost.API.Core
{
    public class ApiControllerBase : ApiController
    {
        protected T ExecuteWithExceptionHandler<T>(Func<T> method) {
            try {
                return method.Invoke();
            } catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw;
            }
        }


        protected void ExecuteWithExceptionHandler(Action action) {
            try
            {
                action.Invoke();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw;
            }
        }
    }
}
