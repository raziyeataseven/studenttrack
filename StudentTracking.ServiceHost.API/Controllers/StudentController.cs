using System;
using System.Web.Http;
using StudentTracking.Data.Contracts.Repository;
using StudentTracking.ServiceHost.API.Core;
using StudtentTracking.Data.Entities;

namespace StudentTracking.ServiceHost.API.Controllers
{
    [Authorize]
    public class StudentController : ApiControllerBase
    {

        private readonly IStudentRepository _studentRepository;
        public StudentController(IStudentRepository studentRepostory)
        {
            this._studentRepository = studentRepostory;
        }

        [HttpGet]
        public Student Get(int? id) 
        {
            return ExecuteWithExceptionHandler(() => 
            {
                return _studentRepository.Get(id ?? 0);
            });
        }

        [HttpGet]
        public System.Collections.Generic.IEnumerable<Student> GetAll() 
        {
            return ExecuteWithExceptionHandler(() => 
            {
                return _studentRepository.GetAll();
            });
        }

        [HttpPost]
        public void Create([FromBody] Student model) 
        {
            ExecuteWithExceptionHandler(() => 
            {
                _studentRepository.Insert(model);
            });
        }

        [HttpPut]
        [Route("api/student/update")]
        public void Update([FromBody]Student model)
        {
            ExecuteWithExceptionHandler(() =>
            {
                _studentRepository.Update(model);
            });
        }

        [HttpDelete]
        [Route("api/student/delete")]
        public void Delete([FromBody]Student model)
        {
            ExecuteWithExceptionHandler(() =>
            {
                _studentRepository.Delete(model);
            });
        }
    }
}
