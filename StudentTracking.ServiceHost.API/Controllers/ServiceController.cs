﻿using StudentTracking.Data.Contracts.Repository;
using StudentTracking.ServiceHost.API.Core;
using StudtentTracking.Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudentTracking.ServiceHost.API.Controllers
{
    [Authorize]
    public class ServiceController : ApiControllerBase
    {
        private readonly IServiceRepository _serviceRepository;

        public ServiceController(IServiceRepository serviceRepository)
        {
            _serviceRepository = serviceRepository;
        }

        [HttpGet]
        [Route("api/service/get/id")]
        public Service Get(int id)
        {
            return ExecuteWithExceptionHandler(() => 
            {
                return _serviceRepository.Get(id);    
            });
        }


        [HttpGet]
        [Route("api/service/getall")]
        public IEnumerable<Service> GetAll()
        {
            return ExecuteWithExceptionHandler(() =>
            {
                return _serviceRepository.GetAll();
            });
        }

        [HttpGet]
        [Route("api/service/getasync/id")]
        public async Task<Service> GetAsync(int id)
        {
            return await ExecuteWithExceptionHandler<Task<Service>>(async () => {
                return await _serviceRepository.GetAsync(id);
            });
        }


        [HttpGet]
        [Route("api/service/getallasync")]
        public async Task<IEnumerable<Service>> GetAllAsync()
        {
            return await ExecuteWithExceptionHandler<Task<IEnumerable<Service>>>(async () =>
            {
                return await _serviceRepository.GetAllAsync();
            });

        }

        [HttpPost]
        [Route("api/service/add")]
        public void Create([FromBody]Service model)
        {
            ExecuteWithExceptionHandler(() => 
            {
                _serviceRepository.Insert(model);
            });
        }

        [HttpPut]
        [Route("api/service/update")]
        public void Update([FromBody]Service model)
        {
            ExecuteWithExceptionHandler(() =>
            {
                _serviceRepository.Update(model);
            });
        }

        [HttpDelete]
        [Route("api/service/delete")]
        public void Delete([FromBody]Service model)
        {
            ExecuteWithExceptionHandler(() =>
            {
                _serviceRepository.Delete(model);
            });
        }
    }
}