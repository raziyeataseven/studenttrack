﻿using StudentTracking.Data.Contracts.Repository;
using StudentTracking.ServiceHost.API.Core;
using StudtentTracking.Data.Entities;
using StudtentTracking.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
//using System.Web.Mvc;

namespace StudentTracking.ServiceHost.API.Controllers
{
    public class ContactController : ApiControllerBase
    {
        private readonly IContactRepository _contactRepository;
        public ContactController(IContactRepository contactRepository)
        {
            _contactRepository = contactRepository;
        }


        [HttpGet]
        [Route("api/contact/get/id")]
        public ContactInformation Get(int id)
        {
            return ExecuteWithExceptionHandler(() =>
            {
                return _contactRepository.Get(id);
            });
        }

        [HttpGet]
        [Route("api/contact/getall")]
        public IEnumerable<ContactInformation> GetAll()
        {
            return ExecuteWithExceptionHandler(() =>
            {
                return _contactRepository.GetAll();
            });
        }

        [HttpGet]
        [Route("api/contact/getallbyuserid")]
        public IEnumerable<ContactInformation> GetAllByUserId()
        {
            return ExecuteWithExceptionHandler(() =>
            {
                return _contactRepository.GetAllByUserId(HttpContext.Current.User);
            });
        }


        [HttpPost]
        [Route("api/contact/add")]
        public void Create([FromBody]ContactInformation model)
        {
            ExecuteWithExceptionHandler(() =>
            {
                return _contactRepository.Insert(model);
            });
        }

        [HttpPut]
        [Route("api/contact/update")]
        public void Update([FromBody]ContactInformation model)
        {
            ExecuteWithExceptionHandler(() => 
            {
                _contactRepository.Update(model);
            });
        }

        [HttpDelete]
        [Route("api/contact/delete")]
        public void Delete([FromBody]ContactInformation model)
        {
            ExecuteWithExceptionHandler(() => 
            {
                _contactRepository.Delete(model);
            });
        }
    }
}