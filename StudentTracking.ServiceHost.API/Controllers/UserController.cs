﻿using StudentTracking.Data.Contracts.Repository;
using StudentTracking.ServiceHost.API.Core;
using StudtentTracking.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudentTracking.ServiceHost.API.Controllers
{
    public class UserController : ApiControllerBase
    {

        private readonly IUsersRepository _userRepository;

        public UserController(IUsersRepository usersRepostory)
        {
            this._userRepository = usersRepostory;
        }

        [HttpGet]
        [Route("api/user/get/id")]
        public User Get(int id)
        {
            return ExecuteWithExceptionHandler(() =>
            {
                return _userRepository.Get(id);
            });
        }


        [HttpGet]
        [Route("api/user/getall")]
        public IEnumerable<User> GetAll()
        {
            return ExecuteWithExceptionHandler(() =>
            {
                return _userRepository.GetAll();
            });
        }

        [HttpGet]
        [Route("api/user/getasync/id")]
        public async Task<User> GetAsync(int id)
        {
           return await ExecuteWithExceptionHandler( async () =>
            {
                return await _userRepository.GetAsync(id);
            });
        }


        [HttpGet]
        [Route("api/user/getallasync")]
        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await ExecuteWithExceptionHandler( async () =>
            {
                return await _userRepository.GetAllAsync();
            });
           
        }


        [HttpPost]
        [Route("api/user/add")]
        public void Add([FromBody]User obj)
        {
            ExecuteWithExceptionHandler(() =>
            {
                _userRepository.Insert(obj);
            });            
        }

        [HttpPut]
        [Route("api/user/update")]
        public void Update([FromBody]User obj)
        {
            ExecuteWithExceptionHandler(() =>
            {
                _userRepository.Update(obj);
            });
           
        }

        [HttpDelete]
        [Route("api/user/update")]
        public void Delete([FromBody]User obj)
        {
            ExecuteWithExceptionHandler(() =>
            {
                _userRepository.Delete(obj);
            });
            
        }
    }
}