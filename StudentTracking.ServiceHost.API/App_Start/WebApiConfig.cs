﻿using System.Web.Http;
using Autofac;
using Newtonsoft.Json.Serialization;
using StudentTracking.ServiceHost.API.Handlers;
using StudtentTracking.Data.Repository;

namespace StudentTracking.ServiceHost.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            // Autofact IoC
            //var builder = new ContainerBuilder();
            //builder.RegisterType<ContactRepository>().As<IContactRepository>().SingleInstance();

            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            config.MessageHandlers.Add(new ApiResponseHandler());
            config.Formatters.JsonFormatter.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
