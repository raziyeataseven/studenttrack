﻿using System;
using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Autofac.Integration.Mvc;
using StudtentTracking.Data.Repository;
using StudentTracking.ServiceHost.API.Controllers;
using System.Web.Mvc;

namespace StudentTracking.ServiceHost.API
{
    public class IoCConfig
    {
        public static void Configure()
        {
            var containerBuilder = new ContainerBuilder();
            //containerBuilder.RegisterControllers(Assembly.GetExecutingAssembly());
            //containerBuilder.RegisterAssemblyTypes(typeof(ServiceController).Assembly).InstancePerRequest();

            containerBuilder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            containerBuilder.RegisterAssemblyTypes(typeof(StudentRepository).Assembly)
                            .Where(s => s.Name.EndsWith("Repository"))
                            .AsImplementedInterfaces()
                            .InstancePerRequest();

            var container = containerBuilder.Build();
            var resolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = resolver;
            //DependencyResolver.SetResolver(new AutofacWebApiDependencyResolver(container));

        }

 
    }
}
