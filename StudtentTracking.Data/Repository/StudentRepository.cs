﻿using StudtentTracking.Data.Entities;
using System;
using System.Collections.Generic;
using StudentTracking.Data.Contracts.Repository;
using System.Threading.Tasks;
using StudtentTracking.Data;
using System.Linq;
using Dapper;
using System.Data;

namespace StudtentTracking.Data.Repository
{

    public class StudentRepository : IStudentRepository
    {

        #region private methods
        private MyDapper.IDapperTools dapper { get; set; }
        public IDbConnection dbContext { get; set; }
        #endregion

        #region constructor
        public StudentRepository()
        {
            dapper = new MyDapper.DapperTools(Constants.dapperContext);
            dbContext = Constants.context;
        }
        #endregion


        #region public methods
        public Student Get(object id)
        {
            return dapper.Get<Student>(id);
        }

        public IEnumerable<Student> GetAll()
        {
            return dapper.GetAll<Student>();
        }

        public long Insert(Student obj)
        {
            return dapper.Insert<Student>(obj);
        }

        public bool Update(Student obj)
        {
            return dapper.Update<Student>(obj);
        }

        public bool Delete(Student obj)
        {
            return dapper.Delete<Student>(obj);
        }

        public async Task<Student> GetAsync(object id)
        {
            return await dapper.GetAsync<Student>(id);
        }

        public async Task<IEnumerable<Student>> GetAllAsync()
        {
            return await dapper.GetAllAsync<Student>();
        }

        public IEnumerable<Student> GetAllByUserId(object userId)
        {
            return dapper.Query<Student>(@"select * from Students where userId=@userId", new { @userId = userId });
        }

        public IEnumerable<Student> GetStudents(string userId)
        {
            var response = new Dictionary<int, StudentResponseModel>();
            var studentResult = dbContext.Query<Student, Location, School, ContactInformation, Student>(
                @"SELECT
	                student.Id, student.studentName, student.gender, student.checkIn, student.checkOut, 
                    student.serviceId, student.schoolId,
                    location.Id, location.locationName,	                
                    school.Id, school.schoolName, 
	                phone.Id, phone.phoneNumber
                FROM
	                Students student WITH(NOLOCK) 
	                LEFT OUTER JOIN Locations location ON location.studentId = student.Id
	                LEFT OUTER JOIN Schools school ON student.schoolId = school.Id
	                LEFT OUTER JOIN PhoneNumbers phone ON phone.studentId = student.Id
                WHERE 
	                student.userId = @userId
                    AND student.deleted = 0 AND location.deleted = 0 AND phone.deleted = 0", 
                (Student, Location, School, ContactInformation) =>
                {
                    Student.Schools.Add(School);
                    Student.Locations.Add(Location);
                    Student.Contacts.Add(ContactInformation);
                    return Student;

                }, new { @userId = userId }, null, true, splitOn: "Id", commandTimeout: 120, commandType: CommandType.Text).ToList();
            return studentResult;
        }


        #endregion
    }
}
