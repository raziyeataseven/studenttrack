﻿using StudentTracking.Data.Contracts.Repository;
using StudtentTracking.Data.MyDapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using StudtentTracking.Data.Entities;

namespace StudtentTracking.Data.Repository
{
    public class LocationRepository : ILocationRepository
    {
        #region private methods
        private IDapperTools dapper { get; set; }
        #endregion

        #region constructor
        public LocationRepository()
        {
            dapper = new DapperTools(Constants.dapperContext);
        }

        #endregion

        #region public methods
        public async Task<Location> GetAsync(object id)
        {
            return await dapper.GetAsync<Location>(id);
        }

        public async Task<IEnumerable<Location>> GetAllAsync()
        {
            return await dapper.GetAllAsync<Location>();
        }

        public Location Get(object id)
        {
            return dapper.Get<Location>(id);
        }

        public IEnumerable<Location> GetAll()
        {
            return dapper.GetAll<Location>();
        }

     

        public long Insert(Location obj)
        {
            return dapper.Insert<Location>(obj);
        }

        public bool Update(Location obj)
        {
            return dapper.Update<Location>(obj);
        }

        public bool Delete(Location obj)
        {
            return dapper.Delete<Location>(obj);
        }

        public IEnumerable<Location> GetAllByUserId(object userId)
        {
            return dapper.Query<Location>(@"Select * from Locations where userId= @userId", new { @userId = userId });

        }

        #endregion

    }
}
