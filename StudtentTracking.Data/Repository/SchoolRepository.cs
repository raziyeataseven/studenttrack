﻿using StudentTracking.Data.Contracts.Repository;
using StudtentTracking.Data.MyDapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudtentTracking.Data.Entities;
using StudentTracking.Data.Contracts;

namespace StudtentTracking.Data.Repository
{
    public class SchoolRepository:ISchoolRepository
    {
        #region private methods
        private IDapperTools dapper { get; set; }
        #endregion

        #region constructor
        public SchoolRepository()
        {
            dapper = new DapperTools(Constants.dapperContext);
        }
        #endregion

        #region public methods
        public School Get(object id)
        {
            return dapper.Get<School>(id);
        }

        public IEnumerable<School> GetAll()
        {
            return dapper.GetAll<School>();
        }
   

        public long Insert(School obj)
        {
            return dapper.Insert<School>(obj);
        }

        public bool Update(School obj)
        {
            return dapper.Update<School>(obj);
        }

        public bool Delete(School obj)
        {
            return dapper.Delete<School>(obj);
        }

        public async Task<School> GetAsync(object id)
        {
            return await dapper.GetAsync<School>(id);
        }

        public async Task<IEnumerable<School>> GetAllAsync()
        {
            return await dapper.GetAllAsync<School>();
        }

        public IEnumerable<School> GetAllByUserId(object userId)
        {
            return dapper.Query<School>(@"select * from Schools where userId=@userId", new { @userId = userId });

        }

        #endregion

    }
}
