﻿using StudentTracking.Data.Contracts.Repository;
using StudtentTracking.Data;
using StudtentTracking.Data.MyDapper;
using StudtentTracking.Data.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudtentTracking.Data.Repository
{
    
    public class ContactRepository : IContactRepository
    {
        #region private methods
        private IDapperTools dapper { get; set; }
        #endregion

        #region constructor
        public ContactRepository()
        {
            dapper = new DapperTools(Constants.dapperContext);
        }
        #endregion


        #region public methods

        public ContactInformation Get(object id)
        {
            return dapper.Get<ContactInformation>(id);
        }

        public IEnumerable<ContactInformation> GetAll()
        {
            return dapper.GetAll<ContactInformation>();
        }

        public long Insert(ContactInformation obj)
        {
            return dapper.Insert<ContactInformation>(obj);
        }

        public bool Update(ContactInformation obj)
        {
            return dapper.Update<ContactInformation>(obj);
        }
        public bool Delete(ContactInformation obj)
        {
            return dapper.Delete<ContactInformation>(obj);
        }
        public async Task<ContactInformation> GetAsync(object id)
        {
            return await dapper.GetAsync<ContactInformation>(id);
        }

        public async Task<IEnumerable<ContactInformation>> GetAllAsync()
        {
            return await dapper.GetAllAsync<ContactInformation>();
        }

        public IEnumerable<ContactInformation> GetAllByUserId(object userId)
        {
            return dapper.Query<ContactInformation>(@"Select * from PhoneNumbers where userId=@userId", new { @userId = userId });

        }
        #endregion
    }
}
