﻿using StudentTracking.Data.Contracts.Repository;
using StudtentTracking.Data.MyDapper;
using StudtentTracking.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudtentTracking.Data.Repository
{
    public class UsersRepository : IUsersRepository
    {
        #region private variables
        private IDapperTools dapper { get; set; }
        #endregion

        #region constructor
        public UsersRepository()
        {
            dapper = new DapperTools(Constants.dapperContext);
        }
        #endregion

        #region public methods       
        public bool Delete(User obj)
        {
            return dapper.Delete<User>(obj);
        }

        public User Get(object id)
        {
            return dapper.Get<User>(id);
        }

        public IEnumerable<User> GetAll()
        {
            return dapper.GetAll<User>();
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await dapper.GetAllAsync<User>();
        }

        public async Task<User> GetAsync(object id)
        {
            return await dapper.GetAsync<User>(id);
        }

        public long Insert(User obj)
        {
            return dapper.Insert<User>(obj);
        }

        public bool Update(User obj)
        {
            return dapper.Update<User>(obj);
        }

        public IEnumerable<User> GetAllByUserId(object userId)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
