﻿using StudentTracking.Data.Contracts.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudtentTracking.Data.Entities;
using StudtentTracking.Data.MyDapper;

namespace StudtentTracking.Data.Repository
{
    public class ServiceRepository : IServiceRepository
    {
        
        #region private methods
        private IDapperTools dapper { get; set; }
        #endregion

        #region constructor
        public ServiceRepository()//DapperContext context
        {
            dapper = new DapperTools(Constants.dapperContext);
        }
        #endregion

        #region public methods
        
        public Service Get(object id)
        {
            return dapper.Get<Service>(id);
        }

        public IEnumerable<Service> GetAll()
        {
            return dapper.GetAll<Service>();
        }

    
        public async Task<IEnumerable<Service>> GetAllAsync()
        {
            return await dapper.GetAllAsync<Service>();
        }

        public async Task<Service> GetAsync(object id)
        {
            return await dapper.GetAsync<Service>(id);
        }

        public long Insert(Service obj)
        {
            return dapper.Insert<Service>(obj);
        }

        public bool Update(Service obj)
        {
            return dapper.Update<Service>(obj);
        }

        public bool Delete(Service obj)
        {
            return dapper.Delete<Service>(obj);
        }

        public IEnumerable<Service> GetAllByUserId(object userId)
        {
            return dapper.Query<Service>(@"Select * from Services where userId=@userId", new { @userId = userId });
        }
        #endregion
    }
}
