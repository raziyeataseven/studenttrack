﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudtentTracking.Data
{
    public class Constants
    {        
        public static MyDapper.IDapperContext dapperContext => new MyDapper.DapperContext(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

        public static IDbConnection context => new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
    }
}
