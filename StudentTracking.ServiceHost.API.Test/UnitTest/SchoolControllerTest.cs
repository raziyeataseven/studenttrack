﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudentTracking.Data.Contracts.Repository;
using StudtentTracking.Data.Repository;
using StudtentTracking.Data.Entities;

namespace StudentTracking.ServiceHost.API.Test.UnitTest
{
    [TestClass]
    public class SchoolControllerTest
    {
        private ISchoolRepository _schoolRep;

        [TestInitialize]
        public void TestInitialize()
        {
            _schoolRep = new SchoolRepository();
        }

        [TestMethod]
        public void AddSchool()
        {
            var school = new School
            {
                createdAt = DateTime.Now,
                schoolName = "Nazilli Anadolu Teknik, Anadolu Meslek, Teknik Lise ve Endüstri Meslek Lisesi",
                deleted=false,
                deletedAt=DateTime.Now
            };

            var isSuccess = _schoolRep.Insert(school);
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            _schoolRep = null;
        }

    }
}
