﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudentTracking.Data.Contracts.Repository;
using StudtentTracking.Data.Repository;
using StudtentTracking.Data.Entities;

namespace StudentTracking.ServiceHost.API.Test.UnitTest
{
    [TestClass]
    public class UsersControllerTest
    {
        private IUsersRepository _usersRep;

        [TestInitialize]
        public void TestInitialize()
        {
            _usersRep = new UsersRepository();
        }

        [TestMethod]
        public void AddUser()
        {
            var obj = new User
            {
                createdAt = DateTime.Now,
                deletedAt = DateTime.Now,
              
            };

            var isSuccess = _usersRep.Insert(obj);
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            _usersRep = null;
        }
    }
}
