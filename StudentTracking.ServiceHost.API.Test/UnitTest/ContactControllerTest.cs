﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudentTracking.Data.Contracts.Repository;
using StudtentTracking.Data.Repository;
using StudtentTracking.Data.Entities;

namespace StudentTracking.ServiceHost.API.Test.UnitTest
{
    [TestClass]
    public class ContactControllerTest
    {
        private IContactRepository _contactRep;

        [TestInitialize]
        public void TestInitialize()
        {
            _contactRep = new ContactRepository();
        }

        [TestMethod]
        public void AddContact()
        {
            var obj = new ContactInformation
            {
              createdAt=DateTime.Now,
              deletedAt=DateTime.Now,
              phoneNumber="2323450000"
            };

            var isSuccess = _contactRep.Insert(obj);
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            _contactRep = null;
        }
    }
}
