﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudentTracking.Data.Contracts.Repository;
using StudtentTracking.Data.Repository;
using StudtentTracking.Data.Entities;

namespace StudentTracking.ServiceHost.API.Test.UnitTest
{
    [TestClass]
    public class ServiceControllerTest
    {
      
        IServiceRepository _serviceRepo;

        [TestInitialize]
        public void TestInitialize()
        {
            _serviceRepo = new ServiceRepository();
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            _serviceRepo = null;
        }

        [TestMethod]
        public void AddService()
        {
            var model = new Service
            {
                createdAt = DateTime.Now,
                deletedAt = DateTime.Now,
                schoolId = 1,
                //schoolName = "Karşıyaka Lisesi"
            };

            var resp = _serviceRepo.Insert(model);
        }

        [TestMethod]
        public void GetServices()
        {
            var services = _serviceRepo.GetAll();
        }
    }
}
