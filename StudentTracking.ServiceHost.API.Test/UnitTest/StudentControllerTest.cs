﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudentTracking.Data.Contracts.Repository;
using StudtentTracking.Data.Repository;
using StudtentTracking.Data.Entities;

namespace StudentTracking.ServiceHost.API.Test.UnitTest
{
    [TestClass]
    public class StudentControllerTest
    {
        private IStudentRepository _studentRep;

        [TestInitialize]
        public void TestInitialize()
        {
            _studentRep = new StudentRepository();
        }

        [TestMethod]
        public void AddStudent()
        {
            var obj = new Student
            {
                createdAt = DateTime.Now,
                deletedAt = DateTime.Now,
                identityNumber = "42100042100",
                parentName = "Fadime",
                studentId = "975",
                studentName = "Raziye",
                schoolId = 1
                //serviceId
            };

            var isSuccess = _studentRep.Insert(obj);
        }


        [TestMethod]
        public void GetStudentJoinObject()
        {
            //var result=_studentRep.GetStudents("cce5afab-5e64-45c5-b115-38487f5e9560");
            var result = _studentRep.GetStudents("732eacfc-f072-44c3-abf6-0726f835fabb");

        }

        [TestCleanup]
        public void TestCleanUp()
        {
            _studentRep = null;
        }
    }
}
