﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudentTracking.Data.Contracts.Repository;
using StudtentTracking.Data.Repository;
using StudtentTracking.Data.Entities;

namespace StudentTracking.ServiceHost.API.Test.UnitTest
{
    [TestClass]
    public class LocationControllerTest
    {
        private ILocationRepository _locationRep;

        [TestInitialize]
        public void TestInitialize()
        {
            _locationRep = new LocationRepository();
        }

        [TestMethod]
        public void AddLocation()
        {
            var obj = new Location
            {
                createdAt = DateTime.Now,
                deletedAt = DateTime.Now,
                latitude = "",
                locationName = "Nazilli Merkez",
                longitude = "",
                deleted = false,
                //studentId = 1
            };

            var isSuccess = _locationRep.Insert(obj);
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            _locationRep = null;
        }
    }
}
