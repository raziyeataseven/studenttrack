﻿using StudentTracking.Data.Contracts.Repository;
using StudtentTracking.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentTracking.WebHost.Helper
{
    public class SelectListItemHelper
    {
        private static readonly ISchoolRepository _schoolRepository = new SchoolRepository();
        private static readonly IServiceRepository _serviceRepository = new ServiceRepository();
        private static readonly IStudentRepository _studentRepository = new StudentRepository();
        private static readonly string userId = UserInfoHelper.GetUserId();
        public SelectListItemHelper()
        {
        }

        internal static List<SelectListItem> SchoolsKeyValues()
        {
           
                var items = new List<SelectListItem>();
                var schoolObj = _schoolRepository.GetAllByUserId(userId);
                foreach (var item in schoolObj)
                {
                    items.Add(new SelectListItem
                    {
                        Text = item.schoolName,
                        Value = item.Id.ToString()
                    });
                }
                HttpContext.Current.Session["SchoolsKeyValues"] = items;
                return items;

        }

        internal static List<SelectListItem> ServicesKeyValues()
        {
            var items = new List<SelectListItem>();
            var serviceObj = _serviceRepository.GetAllByUserId(userId);
            foreach (var item in serviceObj)
            {
                items.Add(new SelectListItem
                {
                    Text = item.serviceName,
                    Value = item.Id.ToString()
                });
            }
            return items;
        }

        internal static List<SelectListItem> GenderKeyValues()
        {
            var items = new List<SelectListItem>();
            items.Add(new SelectListItem
            { Text = "Kadın", Value = "Kadın" });

            items.Add(new SelectListItem
            {
                Text = "Erkek",
                Value = "Erkek"
            });

            return items;
        }

        internal static List<SelectListItem> StudentsKeyValues()
        {
            var students = _studentRepository.GetAllByUserId(userId);
            var studentList = new List<SelectListItem>();
            foreach (var item in students)
            {
                studentList.Add(new SelectListItem
                {
                    Text = item.identityNumber + " " + item.studentName,
                    Value = item.Id.ToString()
                });
            }

            return studentList;
        }

    }
}