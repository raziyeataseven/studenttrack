﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;

namespace StudentTracking.WebHost.Helper
{
    public class UserInfoHelper
    {
        public static string GetUserId()
        {
            return HttpContext.Current.User.Identity.GetUserId();
        }
    }
}