﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StudtentTracking.Data.Entities;
using StudentTracking.Data.Contracts.Repository;
using Microsoft.AspNet.Identity;
using StudentTracking.WebHost.Helper;

namespace StudentTracking.WebHost.Controllers
{
    [Authorize]
    public class StudentsController : Controller
    {
        private readonly IStudentRepository _studentRepository;
        private readonly ISchoolRepository _schoolRepository;
        private readonly IServiceRepository _serviceRepository;

        private readonly string userId = UserInfoHelper.GetUserId();
        public StudentsController(IStudentRepository studentRepository, ISchoolRepository schoolRepository, IServiceRepository serviceRepository)
        {
            _studentRepository = studentRepository;
            _schoolRepository = schoolRepository;
            _serviceRepository = serviceRepository;
        }

        // GET: Students
        public ActionResult Index()
        {
            ViewBag.Schools = _schoolRepository.GetAll();
            ViewBag.Services = _serviceRepository.GetAll();
            return View(_studentRepository.GetAllByUserId(userId).ToList());
        }

        // GET: Students/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = _studentRepository.Get(id);
            if (student == null)
            {
                return HttpNotFound();
            }

            if (student.userId != UserInfoHelper.GetUserId())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            ViewBag.School = _schoolRepository.Get(student.schoolId);
            ViewBag.Service = _serviceRepository.Get(student.serviceId);
            return View(student);
        }

        // GET: Students/Create
        public ActionResult Create()
        {
            ViewBag.SchoolsSelectList = SelectListItemHelper.SchoolsKeyValues();
            ViewBag.ServicesSelectList = SelectListItemHelper.ServicesKeyValues();
            ViewBag.GenderSelectList = SelectListItemHelper.GenderKeyValues();
            return View();
        }

        // POST: Students/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "studentName,studentId,schoolId,serviceId,identityNumber,parentName,checkIn,checkOut,gender")] Student student)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    student.createdAt = DateTime.Now;
                    student.deletedAt = new DateTime(1800, 1, 1);
                    student.userId = HttpContext.User.Identity.GetUserId();
                    _studentRepository.Insert(student);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Kayıt Ekleme İşlemi Başarısız. " + ex.Message;
                    return RedirectToAction("Error","Shared");
                }

            }

            return View(student);
        }

        // GET: Students/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Student student = _studentRepository.Get(id);
            if (student == null)
            {
                return HttpNotFound();
            }

            if (student.userId != UserInfoHelper.GetUserId())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            var schools = SelectListItemHelper.SchoolsKeyValues();
            schools.Where(i => i.Value == student.schoolId.ToString()).First(j => j.Selected = true);

            var services = SelectListItemHelper.ServicesKeyValues();
            services.Where(i => i.Value == student.serviceId.ToString()).First(j => j.Selected = true);

            var genders = SelectListItemHelper.GenderKeyValues();
            if (student.gender != null)
                genders.First(f => f.Value == student.gender).Selected = true;
            else
                student.gender = "";

            ViewBag.Schools = schools;
            ViewBag.Services = services;
            ViewBag.Genders = genders;
            return View(student);
        }

        // POST: Students/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,studentName,studentId,schoolId,serviceId,identityNumber,parentName,checkIn,checkOut,gender,createdAt,deletedAt,deleted")] Student student)
        {
            if (!ModelState.IsValid)
            {
                return View(student);
            }

            try
            {
                var studentTemp = _studentRepository.Get(student.Id);

                if (studentTemp.userId != UserInfoHelper.GetUserId())
                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

                student.userId = UserInfoHelper.GetUserId();
                _studentRepository.Update(student);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Düzenleme İşlemi Başarısız. " + ex.Message;
                return RedirectToAction("Error", "Shared");
            }

        }

        // GET: Students/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = _studentRepository.Get(id);
            if (student == null)
            {
                return HttpNotFound();
            }

            if (student.userId != UserInfoHelper.GetUserId())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            ViewBag.school = _schoolRepository.Get(student.schoolId);
            ViewBag.service = _serviceRepository.Get(student.serviceId);
            return View(student);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Student student = _studentRepository.Get(id);

                if (student.userId != UserInfoHelper.GetUserId())
                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

                _studentRepository.Delete(student);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Silme İşlemi Başarısız. " + ex.Message;
                return RedirectToAction("Error","Shared");
            }

        }

      

    }
}
