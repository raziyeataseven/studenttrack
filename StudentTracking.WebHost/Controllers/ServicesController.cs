﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using StudtentTracking.Data.Entities;
using StudentTracking.Data.Contracts.Repository;
using StudentTracking.WebHost.Helper;

namespace StudentTracking.WebHost.Controllers
{
    [Authorize]
    public class ServicesController : Controller
    {
        private readonly IServiceRepository _serviceRepository;
        private readonly ISchoolRepository _schoolRepository;

        public ServicesController(IServiceRepository serviceRepository,ISchoolRepository schoolRepository)
        {
            _serviceRepository = serviceRepository;
            _schoolRepository = schoolRepository;
        }

        // GET: Services
        public ActionResult Index()
        {
            ViewBag.school = _schoolRepository.GetAll();/*GetAllByUserId(UserInfoHelper.GetUserId());*/
            return View(_serviceRepository.GetAllByUserId(UserInfoHelper.GetUserId()).ToList());
        }


        // GET: Services/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = _serviceRepository.Get(id);
            if (service == null)
            {
                return HttpNotFound();
            }

            if (service.userId != UserInfoHelper.GetUserId())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            ViewBag.school = _schoolRepository.Get(service.schoolId).schoolName;
            return View(service);
        }

        // GET: Services/Create
        public ActionResult Create()
        {
            ViewBag.SchoolsSelectList = SelectListItemHelper.SchoolsKeyValues();
            return View();
        }

        // POST: Services/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "serviceName,schoolId,createdAt,deletedAt,deleted")] Service service)
        {
            if (!ModelState.IsValid)
            {
                return View(service);
            }
            try
            {
                service.userId = UserInfoHelper.GetUserId();
                service.deleted = false;
                service.deletedAt = new System.DateTime(1900, 1, 1);
                service.createdAt = System.DateTime.Now;
                _serviceRepository.Insert(service);
            }
            catch (System.Exception ex)
            {
                TempData["error"] = "Kayıt Ekleme İşlemi Başarısız. " + ex.Message;
                return RedirectToAction("Error", "Shared");
            }
            return RedirectToAction("Index");

        }

        // GET: Services/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = _serviceRepository.Get(id);
            if (service == null)
            {
                return HttpNotFound();
            }

            if (service.userId != UserInfoHelper.GetUserId())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

                var schools = SelectListItemHelper.SchoolsKeyValues();
                schools.First(i => i.Value == service.schoolId.ToString()).Selected = true;
                ViewBag.SchoolsSelectList = schools;
                return View(service);
        
            
        }

        // POST: Services/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,serviceName,schoolId,deleted,createdAt,deletedAt")] Service service)
        {
            if (!ModelState.IsValid)
            {
                return View(service);
            }

            var serviceTemp = _serviceRepository.Get(service.Id);

            if (serviceTemp.userId != UserInfoHelper.GetUserId())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            try
            {
                service.userId = UserInfoHelper.GetUserId();
                _serviceRepository.Update(service);
                return RedirectToAction("Index");
            }
            catch (System.Exception ex)
            {
                TempData["error"] = "Düzenleme İşlemi Başarısız. " + ex.Message;
                return RedirectToAction("Error", "Shared");
            }
            
        }

        // GET: Services/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = _serviceRepository.Get(id);
            if (service == null)
            {
                return HttpNotFound();
            }

            if (service.userId != UserInfoHelper.GetUserId())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            ViewBag.school = _schoolRepository.Get(service.schoolId).schoolName;
            return View(service);
        }

        // POST: Services/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Service service = _serviceRepository.Get(id);

            if (service.userId != UserInfoHelper.GetUserId())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            try
            {
                _serviceRepository.Delete(service);
                return RedirectToAction("Index");
            }
            catch (System.Exception ex)
            {
                TempData["error"] = "Silme İşlemi Başarısız. " + ex.Message;
                return RedirectToAction("Error", "Shared");
            }
            
        }

       
    }
}
