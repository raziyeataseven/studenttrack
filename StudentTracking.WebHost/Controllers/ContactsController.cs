﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StudtentTracking.Data.Entities;
using StudentTracking.Data.Contracts.Repository;
using Microsoft.AspNet.Identity;
using StudentTracking.WebHost.Helper;

namespace StudentTracking.WebHost.Controllers
{
    [Authorize]
    public class ContactsController : Controller
    {
        private readonly IContactRepository _contactRepository;
        private readonly IStudentRepository _studentRepository;               
        public ContactsController(IContactRepository contactRepository, IStudentRepository studentRepository)
        {
            _contactRepository = contactRepository;
            _studentRepository = studentRepository;
        }

        // GET: ContactInformations
        public ActionResult Index()
        {
            ViewBag.Students = _studentRepository.GetAllByUserId(UserInfoHelper.GetUserId());
            return View(_contactRepository.GetAllByUserId(UserInfoHelper.GetUserId()).ToList());
        }

        // GET: ContactInformations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactInformation contactInformation = _contactRepository.Get(id);
            if (contactInformation == null)
            {
                return HttpNotFound();
            }

            if (contactInformation.userId != UserInfoHelper.GetUserId())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            ViewBag.studentName = _studentRepository.Get(contactInformation.studentId).studentName;
            return View(contactInformation);
        }

        // GET: ContactInformations/Create
        public ActionResult Create()
        {
            ViewBag.StudentSelectList = SelectListItemHelper.StudentsKeyValues();
            return View();
        }

        // POST: ContactInformations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "studentId,phoneNumber,createdAt,deletedAt,deleted")] ContactInformation contactInformation)
        {
            if (!ModelState.IsValid)
            {
                 return View(contactInformation);
            }

            try
            {
                contactInformation.createdAt = DateTime.Now;
                contactInformation.deletedAt = new DateTime(1900, 1, 1);
                contactInformation.userId = HttpContext.User.Identity.GetUserId();
                _contactRepository.Insert(contactInformation);
            }
            catch (Exception ex)
            {
                TempData["error"] = "Kayıt Ekleme İşlemi Başarısız. " + ex.Message;
                return RedirectToAction("Error", "Shared");
            }
                return RedirectToAction("Index");

        }

        // GET: ContactInformations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactInformation contactInformation = _contactRepository.Get(id);
            if (contactInformation == null)
            {
                return HttpNotFound();
            }

            if (contactInformation.userId != UserInfoHelper.GetUserId())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            ViewBag.StudentName = _studentRepository.Get(contactInformation.studentId).studentName;
            return View(contactInformation);
        }

        // POST: ContactInformations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,studentId,phoneNumber,createdAt,deletedAt,deleted")] ContactInformation contactInformation)
        {
            if (ModelState.IsValid)
            {
                var contactTemp = _contactRepository.Get(contactInformation.Id);
                if (contactTemp.userId != UserInfoHelper.GetUserId())
                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

                _contactRepository.Update(contactInformation);
                return RedirectToAction("Index");
            }
            return View(contactInformation);
        }

        // GET: ContactInformations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactInformation contactInformation = _contactRepository.Get(id);
            if (contactInformation == null)
            {
                return HttpNotFound();
            }

            if (contactInformation.userId != UserInfoHelper.GetUserId())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            ViewBag.StudentName = _studentRepository.Get(contactInformation.studentId).studentName;
            return View(contactInformation);
        }

        // POST: ContactInformations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                ContactInformation contactInformation = _contactRepository.Get(id);

                if (contactInformation.userId != UserInfoHelper.GetUserId())
                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

                _contactRepository.Delete(contactInformation);
            }
            catch (Exception ex)
            {
                throw new Exception("Silme işlemi başarısız! "+ ex.Message);
            }
          
            return RedirectToAction("Index");
        }

    }
}
