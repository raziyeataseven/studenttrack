﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StudtentTracking.Data.Entities;
using StudentTracking.Data.Contracts.Repository;
using Microsoft.AspNet.Identity;
using StudentTracking.WebHost.Helper;

namespace StudentTracking.WebHost.Controllers
{
    [Authorize]
    public class LocationsController : Controller
    {
        private readonly ILocationRepository _locationRepository;
        private readonly IStudentRepository _studentRepository;
        public LocationsController(ILocationRepository locationRepository, IStudentRepository studentRepository)
        {
            _locationRepository = locationRepository;
            _studentRepository = studentRepository;
        }
        // GET: Locations
        public ActionResult Index()
        {
            ViewBag.students = _studentRepository.GetAll();
            return View(_locationRepository.GetAllByUserId(UserInfoHelper.GetUserId()).ToList());
        }

        // GET: Locations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = _locationRepository.Get(id);
            if (location == null)
            {
                return HttpNotFound();
            }

            if (location.userId != UserInfoHelper.GetUserId())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            return View(location);
        }

        // GET: Locations/Create
        public ActionResult Create()
        {
            ViewBag.students = Helper.SelectListItemHelper.StudentsKeyValues();
            return View();
        }

        // POST: Locations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "studentId,locationName,latitude,longitude,createdAt,deletedAt,deleted")] Location location)
        {
            if (!ModelState.IsValid)
                return View(location);

            try
            {
                location.userId = HttpContext.User.Identity.GetUserId();
                location.deleted = false;
                location.deletedAt = new DateTime(1900, 1, 1);
                location.createdAt = DateTime.Now;
                _locationRepository.Insert(location);
            }
            catch (Exception ex)
            {
                TempData["error"] = "Kayıt Eklenemedi. " + ex.Message;
                return RedirectToAction("Error", "Shared");
            }
                return RedirectToAction("Index");

        }

        // GET: Locations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = _locationRepository.Get(id);
            if (location == null)
            {
                return HttpNotFound();
            }

            if (location.userId != UserInfoHelper.GetUserId())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            return View(location);
        }

        // POST: Locations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,studentId,locationName,latitude,longitude,createdAt,deletedAt,deleted")] Location location)
        {
            if (!ModelState.IsValid)
                return View(location);

            try
            {
                var locationTemp = _locationRepository.Get(location.Id);

                if (locationTemp.userId != UserInfoHelper.GetUserId())
                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

                _locationRepository.Update(location);
            }
            catch (Exception ex)
            {
                TempData["error"] = "Düzenleme İşlemi Başarısız. " + ex.Message;
                return RedirectToAction("Error", "Shared");
            }

            return RedirectToAction("Index");
        }

        // GET: Locations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = _locationRepository.Get(id);
            if (location == null)
            {
                return HttpNotFound();
            }

            if (location.userId != UserInfoHelper.GetUserId())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            return View(location);
        }

        // POST: Locations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Location location = _locationRepository.Get(id);

                if (location.userId != UserInfoHelper.GetUserId())
                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

                _locationRepository.Delete(location);
            }
            catch (Exception ex)
            {
                TempData["error"] = "Silme İşlemi Başarısız. " + ex.Message;
                return RedirectToAction("Error", "Shared");
            }

            return RedirectToAction("Index");
        }

    }
}
