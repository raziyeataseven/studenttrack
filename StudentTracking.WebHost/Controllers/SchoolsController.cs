﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StudtentTracking.Data.Entities;
using StudentTracking.Data.Contracts.Repository;
using Microsoft.AspNet.Identity;
using StudentTracking.WebHost.Helper;

namespace StudentTracking.WebHost.Controllers
{
    [Authorize]
    public class SchoolsController : Controller
    {

        private readonly ISchoolRepository _schoolRepository;
        public SchoolsController(ISchoolRepository schoolRepository)
        {
            _schoolRepository = schoolRepository;
        }
        // GET: Schools
        public ActionResult Index()
        {
            return View(_schoolRepository.GetAllByUserId(UserInfoHelper.GetUserId()).ToList());
        }

        // GET: Schools/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            School school = _schoolRepository.Get(id);

            if (school == null)
            {
                return HttpNotFound();
            }

            if (school.userId != UserInfoHelper.GetUserId())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            return View(school);
        }

        // GET: Schools/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Schools/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "schoolName,createdAt,deletedAt,deleted")] School school)
        {
            if (ModelState.IsValid)
            {
                school.deleted = false;
                school.deletedAt = new DateTime(1900, 1, 1);
                school.createdAt = DateTime.Now;
                school.userId = HttpContext.User.Identity.GetUserId();
                _schoolRepository.Insert(school);
                return RedirectToAction("Index");
            }

            return View(school);
        }

        // GET: Schools/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            School school = _schoolRepository.Get(id);
            if (school == null)
            {
                return HttpNotFound();
            }

            if (school.userId != UserInfoHelper.GetUserId())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            return View(school);
        }

        // POST: Schools/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,schoolName,createdAt,deletedAt,deleted")] School school)
        {
            if (!ModelState.IsValid)
            {
                return View(school);
            }

            var schoolTemp = _schoolRepository.Get(school.Id);
            if (schoolTemp.userId != UserInfoHelper.GetUserId())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            try
            {
                _schoolRepository.Update(school);
            }
            catch (Exception ex)
            {
                TempData["error"] = "Düzenleme işlemi başarısız. " + ex.Message;
                return RedirectToAction("Error", "Shared");
            }
            return RedirectToAction("Index");

        }

        // GET: Schools/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            School school = _schoolRepository.Get(id);
            if (school == null)
            {
                return HttpNotFound();
            }

            if (school.userId != UserInfoHelper.GetUserId())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            return View(school);
        }

        // POST: Schools/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                School school = _schoolRepository.Get(id);

                if (school.userId != UserInfoHelper.GetUserId())
                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

                _schoolRepository.Delete(school);
            }
            catch (Exception ex)
            {
                throw new Exception("Silme işlemi başarısız! " + ex.Message);

            }
            return RedirectToAction("Index");

        }

    }
}
