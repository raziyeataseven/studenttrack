﻿using System;
using System.Reflection;
using Autofac;
using Autofac.Integration.Mvc;
using StudtentTracking.Data.Repository;
using System.Web.Mvc;
using StudentTracking.WebHost.Helper;

namespace StudentTracking.WebHost
{
    public class IoCConfig
    {
        public static void Configure()
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterControllers(Assembly.GetExecutingAssembly());
            containerBuilder.RegisterAssemblyTypes(typeof(StudentRepository).Assembly)
                            .Where(s => s.Name.EndsWith("Repository"))
                            .AsImplementedInterfaces()
                            .InstancePerRequest();

            // containerBuilder.RegisterAssemblyTypes(typeof(SelectListItemHelper).Assembly).SingleInstance();

            var container = containerBuilder.Build();
            var resolver = new AutofacDependencyResolver(container);
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

        }
    }
}
