﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentTracking.Data.Contracts
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<TEntity> GetAsync(object id);

        Task<IEnumerable<TEntity>> GetAllAsync();

        TEntity Get(object id);
        IEnumerable<TEntity> GetAll();

        IEnumerable<TEntity> GetAllByUserId(object userId);
            
        long Insert(TEntity obj);

        bool Update(TEntity obj);

        bool Delete(TEntity obj);

    }
}
