﻿using StudtentTracking.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentTracking.Data.Contracts.Repository
{
    public interface IStudentRepository : IRepository<Student>
    {
        //Dictionary<int, StudentResponseModel> GetStudents(string userId);
        IEnumerable<Student> GetStudents(string userId);
    }
}
