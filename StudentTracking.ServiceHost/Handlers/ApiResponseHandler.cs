﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using StudtentTracking.Common;
using Newtonsoft.Json;

namespace StudentTracking.ServiceHost.Handlers
{
    public class ApiResponseHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var response = await base.SendAsync(request, cancellationToken);
            return BuildApiResponse(request, response);
        }

        HttpResponseMessage BuildApiResponse(HttpRequestMessage request, HttpResponseMessage response) 
        {
            ApiResponse<object> apiResponse = new ApiResponse<object>();
            apiResponse.Header.success = response.IsSuccessStatusCode;

            object content = null;
            string errorMessage = String.Empty;

            if (response.TryGetContentValue(out content) && !response.IsSuccessStatusCode) {
                HttpError httpError = content as HttpError;
                if (httpError != null) {
                    content = null;
                    errorMessage = httpError.ExceptionMessage;
                }
                apiResponse.Header.errorMessage = httpError;
            } else {
                apiResponse.Body = content;
            }

            var newResponse = request.CreateResponse(System.Net.HttpStatusCode.OK, apiResponse);
            foreach (var header in response.Headers)
            {
                newResponse.Headers.Add(header.Key, header.Value);
            }
            return newResponse;
        }
    }
}
