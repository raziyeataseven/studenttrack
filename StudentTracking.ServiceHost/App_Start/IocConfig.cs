﻿using System;
using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Autofac.Integration.Mvc;
using StudtentTracking.Data.Repository;
using StudentTracking.ServiceHost.Controllers;
using System.Web.Mvc;

namespace StudentTracking.ServiceHost
{
    public class IoCConfig
    {
        public static void Configure()
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            containerBuilder.RegisterAssemblyTypes(typeof(StudentRepository).Assembly)
                            .Where(s => s.Name.EndsWith("Repository"))
                            .AsImplementedInterfaces()
                            .InstancePerApiRequest();

            var container = containerBuilder.Build();
            var resolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = resolver;
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

        }
    }
}
