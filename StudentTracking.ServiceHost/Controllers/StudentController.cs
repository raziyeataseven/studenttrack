using System;
using System.Web.Http;
using StudentTracking.Data.Contracts.Repository;
using StudentTracking.ServiceHost.Core;
using StudtentTracking.Data.Entities;
using System.Collections.Generic;
using System.Web;

namespace StudentTracking.ServiceHost.Controllers
{
    [Authorize]
    public class StudentController : ApiControllerBase
    {

        private readonly IStudentRepository _studentRepository;
        public StudentController(IStudentRepository studentRepostory)
        {
            this._studentRepository = studentRepostory;
        }

        [HttpGet]
        public Student Get(int? id) 
        {
            return ExecuteWithExceptionHandler(() => 
            {
                return _studentRepository.Get(id ?? 0);
            });
        }

        [HttpGet]
        [Route("api/student/getall")]
        public IEnumerable<Student> GetAll() 
        {
            return ExecuteWithExceptionHandler(() => 
            {
                if (HttpContext.Current.User.IsInRole("Administrator"))
                {
                    return _studentRepository.GetAll();
                }
                else
                {
                    return _studentRepository.GetAllByUserId(base.GetUserId());
                }
            });
        }

        [HttpGet, Route("api/student/list")]
        public IEnumerable<Student> GetStudentInformation()
        {
           return ExecuteWithExceptionHandler(() =>
            {
                return _studentRepository.GetStudents(base.GetUserId());
            });
        }

        [HttpPost]
        public void Create([FromBody] Student model) 
        {
            ExecuteWithExceptionHandler(() => 
            {
                _studentRepository.Insert(model);
            });
        }

        [HttpPut]
        [Route("api/student/update")]
        public void Update([FromBody]Student model)
        {
            ExecuteWithExceptionHandler(() =>
            {
                _studentRepository.Update(model);
            });
        }

        [HttpDelete]
        [Route("api/student/delete")]
        public void Delete([FromBody]Student model)
        {
            ExecuteWithExceptionHandler(() =>
            {
                _studentRepository.Delete(model);
            });
        }
    }
}
