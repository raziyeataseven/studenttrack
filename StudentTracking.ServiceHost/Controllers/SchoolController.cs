﻿using StudentTracking.Data.Contracts.Repository;
using StudentTracking.ServiceHost.Core;
using StudtentTracking.Data.Entities;
using StudtentTracking.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
//using System.Web.Mvc;

namespace StudentTracking.ServiceHost.Controllers
{
    public class SchoolController : ApiControllerBase
    {
        private readonly ISchoolRepository _schoolRepository;
        public SchoolController(ISchoolRepository schoolRepository)
        {
            _schoolRepository = schoolRepository;
        }

        [HttpGet]
        [Route("api/school/get/id")]
        public School Get(int id)
        {
            return ExecuteWithExceptionHandler(() => 
            {
                return _schoolRepository.Get(id);
            });
        }

        [HttpGet]
        [Route("api/school/getall")]
        public IEnumerable<School> GetAll()
        {
            return ExecuteWithExceptionHandler(() =>
            {
                if (HttpContext.Current.User.IsInRole("Administrator"))
                {
                    return _schoolRepository.GetAll();
                }
                else
                {
                    return _schoolRepository.GetAllByUserId(base.GetUserId());
                }                
            });
        }


        [HttpPost]
        [Route("api/school/add")]
        public void Create([FromBody]School model)
        {
            ExecuteWithExceptionHandler(() =>
            {
                _schoolRepository.Insert(model);
            });
        }

        [HttpPut]
        [Route("api/school/update")]
        public void Update([FromBody]School model)
        {
            ExecuteWithExceptionHandler(() =>
            {
                _schoolRepository.Update(model);
            });
        }

        [HttpDelete]
        [Route("api/school/delete")]
        public void Delete([FromBody]School model)
        {
            ExecuteWithExceptionHandler(() =>
            {
                _schoolRepository.Delete(model);
            });
        }
    }
}