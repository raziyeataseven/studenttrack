﻿using StudentTracking.Data.Contracts.Repository;
using StudentTracking.ServiceHost.Core;
using StudtentTracking.Data.Entities;
using StudtentTracking.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace StudentTracking.ServiceHost.Controllers
{
    public class LocationController : ApiControllerBase
    {
        private readonly ILocationRepository _locationRepository;
        public LocationController(ILocationRepository locationRepository)
        {
            _locationRepository = locationRepository;
        }

        [HttpGet]
        [Route("api/location/get/id")]
        public Location Get(int id)
        {
            return ExecuteWithExceptionHandler(() => 
            {
                return _locationRepository.Get(id); 
            });
        }

        [HttpGet]
        [Route("api/location/getall")]
        public IEnumerable<Location> GetAll()
        {
            return ExecuteWithExceptionHandler(() =>
            {
                if (HttpContext.Current.User.IsInRole("Administrator"))
                {
                    return _locationRepository.GetAll();
                } else
                {
                    return _locationRepository.GetAllByUserId(base.GetUserId());
                }
            });
        }

        [HttpPost]
        [Route("api/location/add")]
        public void Create([FromBody]Location model)
        {
            ExecuteWithExceptionHandler(() =>
            {
                _locationRepository.Insert(model);
            });
        }

        [HttpPut]
        [Route("api/location/update")]
        public void Update([FromBody]Location model)
        {
            ExecuteWithExceptionHandler(() => 
            {
                _locationRepository.Update(model);
            });
        }

        [HttpDelete]
        [Route("api/location/delete")]
        public void Delete([FromBody]Location model)
        {
            ExecuteWithExceptionHandler(() => 
            {
                _locationRepository.Delete(model);
            });
        }
    }
}