﻿using Dapper.Contrib.Extensions;
using System;
using System.Runtime.Serialization;

namespace StudtentTracking.Data.Entities
{
    public class EntityBase
    {
        [Key]
        public int Id { get; set;  }

        [IgnoreDataMember]
        public DateTime createdAt { get; set; }

        [IgnoreDataMember]
        public DateTime deletedAt { get; set; }

        [IgnoreDataMember]
        public bool deleted { get; set; }

        [IgnoreDataMember]
        public string userId { get; set; }

    }
}
