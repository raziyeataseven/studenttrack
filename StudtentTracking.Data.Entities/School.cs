﻿using Dapper.Contrib.Extensions;
using System;
using System.ComponentModel.DataAnnotations;

namespace StudtentTracking.Data.Entities
{
    [Table("Schools")]
    public class School : EntityBase
    {
        [Display(Name = "Okul")]
        public string schoolName { get; set; }
    }
}
