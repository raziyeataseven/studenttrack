﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudtentTracking.Data.Entities
{
    [Table("Users")]
   public class User:EntityBase
    {

        [Display(Name = "Kullanıcı Adı")]
        public string username { get; set; }

        [Display(Name = "Parola")]
        public string password { get; set; }

        [Display(Name = "Email")]
        public string email { get; set; }

        [Display(Name = "Rol")]
        public string role { get; set; }

    }
}
