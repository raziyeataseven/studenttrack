﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StudtentTracking.Data.Entities
{
    [Dapper.Contrib.Extensions.Table("Services")]
    public class Service : EntityBase
    {
        [Required]
        [Display(Name = "Servis")]
        public string serviceName { get; set; }

        [Required]
        [Display(Name = "Okul")]
        public int schoolId { get; set; }
    }
}
