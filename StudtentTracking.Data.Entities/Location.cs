﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StudtentTracking.Data.Entities
{
    [Dapper.Contrib.Extensions.Table("Locations")]
    public class Location:EntityBase
    {
       
        [Required]
        [Display(Name = "Öğrenci Id")]
        public int studentId { get; set; }

        [Required]
        [Display(Name = "Lokasyon Adı")]
        public string locationName { get; set; }

        [Display(Name = "Enlem")]
        public string latitude { get; set; }

        [Display(Name = "Boylam")]
        public string longitude { get; set; }

        [Display(Name ="Açıklama")]
        public string description { get; set; }
    }
}
