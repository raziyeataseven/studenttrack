﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace StudtentTracking.Data.Entities
{
    [Dapper.Contrib.Extensions.Table("Students")]
    public class Student : EntityBase
    {
        public Student()
        {
            this.Locations = new List<Location>();
            this.Schools = new List<School>();
            this.Contacts = new List<ContactInformation>();
        }
        [Required]
        [Display(Name = "Öğrenci Adı")]
        public string studentName { get; set; }

        [Required]
        [Display(Name = "Öğrenci Numarası")]
        public string studentId { get; set; }

        [Required]
        [Display(Name = "Okul")]
        public int schoolId { get; set; }

        [Required]
        [Display(Name = "Servis")]
        public int serviceId { get; set; }

        [Display(Name = "Kimlik Numarası")]
        public string identityNumber { get; set; }

        [Display(Name = "Ebeveyn Adı")]
        public string parentName { get; set; }

        [Display(Name ="Biniş Zamanı")]
        public TimeSpan checkIn { get; set; }

        [Display(Name ="İniş Zamanı")]
        public TimeSpan checkOut { get; set; }

        [Display(Name ="Cinsiyet")]
        public string gender { get; set; }

        public virtual ICollection<Location> Locations { get; set; }

        public virtual ICollection<School> Schools { get; set; }

        public virtual ICollection<ContactInformation> Contacts { get; set; }
    }

    public class StudentResponseModel
    {
        public string studentName { get; set; }
        public string address { get; set; }
        public string schoolName { get; set; }
        public string gender { get; set; }
        public string checkInTime { get; set; }
        public string checkOutTime { get; set; }
        public List<string> phoneNumbers { get; set; }
    }
}
