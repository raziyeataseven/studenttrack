﻿using Dapper.Contrib.Extensions;
using System;
using System.ComponentModel.DataAnnotations;

namespace StudtentTracking.Data.Entities
{
    [Table("PhoneNumbers")]
    public class ContactInformation : EntityBase
    {
        [Display(Name = "Öğrenci Id")]
        [Required]
        public int studentId { get; set; }

        [Display(Name = "Telefon Numarası")]
        [Required]
        public string phoneNumber { get; set; }

    }
}
