﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AutoMapper;

namespace StudtentTracking.Common
{
    [DataContract]
    public class ApiResponse<T>
    {
        [DataMember]
        public ApiHeader Header;

        [DataMember]
        public T Body;

        public ApiResponse() {
            this.Header = new ApiHeader();
        }

        public ApiResponse(bool isSuccess, T body)
        {
            this.Header = new ApiHeader();
            this.Header.success = isSuccess;
            this.Body = body;
        }
    }
}
